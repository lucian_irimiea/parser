<?php

//Change php.ini settings
ini_set('max_execution_time', 3000);
ini_set('memory_limit', -1);

require_once "init.php";

//Open file
$fisier = fopen("Input/" . $file, "r");

//Generate header---------------------------------------------------------------
$line_number = 0;
while (!feof($fisier)) {
    $line = trim(fgets($fisier)); //Get line from the file, clean blank spaces from begining and end

    @$file_head = $file_head . $line . "\n";

    $line_header = $line;
    $line_number++;

    //generate header
    if ($line_number == $header) {
        $headers = preg_split('/\s+/', $line);
        $columns = count($headers);
        break;
    }

    $line_old = $line;
}
//End generate header-----------------------------------------------------------
//
//Prepare output files of queries and constraints for them----------------------
$file = explode(".", $file);
@$dir = mkdir("Output/" . $file[0]);
for ($i = 0; $i < $query_number; $i++) {
    $j = $i + 1;
    do {
        $query[$i] = fopen("Output/" . $file[0] . "/query_" . $j . ".txt", "w");
    } while ($query[$i] == false);
    $query_err[$i] = 0;
    if (isset($query_comment[$i + 1]))
        fwrite($query[$i], sprintf("%s\n", $query_comment[$i + 1]));
    fwrite($query[$i], sprintf("%s\n\n", $query_constraints[$i + 1]));
    //Create constraint from string
    if (strpos($query_constraints[$i + 1], "max") === 0) {
        $constraint_aux[$i] = $query_constraints[$i + 1];
        $constraint_aux[$i] = str_replace("max(", "", $constraint_aux[$i]);
        $constraint_aux[$i] = str_replace(")", "", $constraint_aux[$i]);
        for ($k = 0; $k < count($headers); $k++) {
            if ($constraint_aux[$i] == $headers[$k]) {
                $col[$i] = $k;
                break;
            }
        }
    } else {
        fwrite($query[$i], sprintf("%s", $file_head));
        $constraint_aux[$i] = preg_split('/\s+/', $query_constraints[$i + 1]);
        for ($j = 0; $j < count($constraint_aux[$i]); $j++) {
            $constraint_aux[$i][$j] = str_replace("(", "", $constraint_aux[$i][$j]);
            $constraint_aux[$i][$j] = str_replace(")", "", $constraint_aux[$i][$j]);
            for ($k = 0; $k < count($headers); $k++) {
                if ($constraint_aux[$i][$j] == $headers[$k]) {
                    $col[$i][$j] = $k;
                    break;
                } else {
                    $col[$i][$j] = $constraint_aux[$i][$j];
                }
            }
        }
    }
}
//End prepare-------------------------------------------------------------------

while (!feof($fisier)) {
    $line = trim(fgets($fisier)); //Get line from the file, clean blank spaces from begining and end

    $line_aux = preg_split('/\s+/', $line);

    $line_number++;
    //Check if relevant line
    if (@$line_aux[1] != $headers[1]) {
        //Start of queries------------------------------------------------------
        for ($i = 0; $i < $query_number; $i++) {
            //Check if relevant line
            if (count($line_aux) != $columns) {
                for ($i = 0; $i < $query_number; $i++) {
                    if (strpos($query_constraints[$i + 1], "max") === false) {
                        fwrite($query[$i], sprintf("%s\n", $line));
                    }
                }
                break;
            }

            //Generate query constraint
            if (strpos($query_constraints[$i + 1], "max") === 0) {
                $max_col[$i][$line_number] = $line_aux[$col[$i]];
            } else {
                $constraint[$i] = "";
                for ($j = 0; $j < count($constraint_aux[$i]); $j++) {
                    if (is_int($col[$i][$j])) {
                        if ($j % 4 == 0)
                            $constraint[$i] = $constraint[$i] . "(" . $line_aux[$col[$i][$j]];
                        elseif ($j % 4 == 2)
                            $constraint[$i] = $constraint[$i] . $line_aux[$col[$i][$j]] . ")";
                    }
                    else {
                        if ($j % 4 == 0)
                            $constraint[$i] = $constraint[$i] . "(" . $col[$i][$j];
                        elseif ($j % 4 == 2)
                            $constraint[$i] = $constraint[$i] . $col[$i][$j] . ")";
                        else
                            $constraint[$i] = $constraint[$i] . " " . $col[$i][$j] . " ";
                    }
                }

                //Write the output files
                if ($constraint[$i] != "") {
                    if (eval("return " . $constraint[$i] . ";")) {
                        if ($file_type == "ceva")
                            fwrite($query[$i], sprintf("%s\n", $line_old));
                        fwrite($query[$i], sprintf("%s\n", $line));
                        $query_err[$i] ++;
                    }
                }
            }
        }
        //End of queries--------------------------------------------------------
    }
    else {
        for ($i = 0; $i < $query_number; $i++) {
            if (strpos($query_constraints[$i + 1], "max") === false) {
                fwrite($query[$i], sprintf("%s\n", $line));
            }
        }
    }
    $line_old = $line;
}

//Write number of errors
for ($i = 0; $i < $query_number; $i++) {
    if (strpos($query_constraints[$i + 1], "max") === false) {
        fwrite($query[$i], sprintf("Number of errors of this type: %s", $query_err[$i]));
    }
}

//Write max values
for ($i = 0; $i < $query_number; $i++) {
    if (strpos($query_constraints[$i + 1], "max") === 0 && isset($max_col[$i])) {
        if (max($max_col[$i]) == 0) {
            fwrite($query[$i], sprintf("The value of the column is 0 on all columns\n"));
        } else {
            if (strpos($headers[$col[$i]], "%") !== false) {
                fwrite($query[$i], sprintf("Max value of column: %s\nOn lines: \n", max($max_col[$i])));
            } else {
                fwrite($query[$i], sprintf("Max " . $headers[$col[$i]] . ": %s\nOn lines: \n", max($max_col[$i])));
            }
            $maxs = array_keys($max_col[$i], max($max_col[$i]));
            for ($j = 0; $j < count($maxs); $j++) {
                fwrite($query[$i], sprintf("%s\n", $maxs[$j]));
            }
        }
    }
}

//Close input file
fclose($fisier);

//Close output files of queries
for ($i = 0; $i < $query_number; $i++) {
    fclose($query[$i]);
}

//Remove unnecessary headers from file------------------------------------------
@$dir = mkdir("Output/" . $file[0] . "/refined");
for ($i = 0; $i < $query_number; $i++) {
    $j = $i + 1;
    do {
        $query[$i] = fopen("Output/" . $file[0] . "/query_" . $j . ".txt", "r");
    } while ($query[$i] == false);
    do {
        $query_ref[$i] = fopen("Output/" . $file[0] . "/refined/query_" . $j . ".txt", "w");
    } while ($query_ref[$i] == false);
    if (isset($line_old_q)) {
        unset($line_old_q);
    }
    if (strpos($query_constraints[$i + 1], "max") === false) {
        while (!feof($query[$i])) {
            $line = trim(fgets($query[$i]));    //Get line from file
            $line_aux_q = $line;
            if ($line != "") {
                $line_aux_q = preg_split('/\s+/', $line);
                unset($line_aux_q[0]);
                $line_aux_q = implode(" ", $line_aux_q);
            }
            if (isset($line_old_q)) {
                if ($line_aux_q != $line_old_q) {
                    fwrite($query_ref[$i], sprintf("%s\n", $line_old));
                }
            }
            $line_old_q = $line_aux_q;
            $line_old = $line;
        }
        fwrite($query_ref[$i], sprintf("%s", $line_old));
    } else {
        while (!feof($query[$i])) {
            $line = trim(fgets($query[$i]));    //Get line from file
            fwrite($query_ref[$i], sprintf("%s\n", $line));
        }
    }
    fclose($query_ref[$i]);
    fclose($query[$i]);
}
//End remove--------------------------------------------------------------------
//
//Reset php.ini settings
ini_set('max_execution_time', 300);
ini_set('memory_limit', '16M');