### What is this repository for? ###

This is a parser for txt files that contain tables of numerical information, as statistics and performance stats for servers.

### How do I get set up? ###

In the files dir, you must create two directories: Input and Output.
The input files should be placed in the Input dir

You can add a varied number of basic querys for the input file.
This is done in the init.php file. More details in the file.
The init.php file must be set up for the file you wish to parse.