<?php

//Init file for script
//By Luci for Ana :)
//The results will be created in the folder with the sama name as the given file
//Refined dir contains the same results, but without some of th redundant lines

//Path to file
//Change file_name with the file name if in the same dir, or full path otherwise
//Ex: 'fisier-1.txt'
$file = 'iostat.out';

//File type
$file_type = 'mpstat';

//Line at which the table header is first found in file
$header = 7;

//=================================================================================
//Enter number of queries
$query_number = 1;

//Parameters for queries
//Used to generate the queries
//The queries will return lines where the specifications are met

//---------------------------------------------------------------------------------
//Start of query 1 values
//Example

//Comment related to the query
//Will be added at beginning of output file
//$query_comment[1] = 'Something, somethin, dark side';   //Do not add if no comment is wanted

//Collumns to compare
//Structure of string:	"'column 1' 'operation' 'column 2'" or "max('column')"
//Operations: < / <= / > / >= / == / != / ===(identical) / !==(not identical)
//Second column can be a number, to compare the firs column to a max or min value
$query_constraints[1] = 'max(svc_t)';

//If you still need multiple constraints in query, write them like so
//$query_constraints[number of query] = '(constraint 1) && (constraint 2) && ...'

//End of query 1 values
//---------------------------------------------------------------------------------

//---------------------------------------------------------------------------------
//Start query 2 values


//End of query 2 values
//---------------------------------------------------------------------------------

//---------------------------------------------------------------------------------
//Start query 3 values


//End of query 3 values
//---------------------------------------------------------------------------------
